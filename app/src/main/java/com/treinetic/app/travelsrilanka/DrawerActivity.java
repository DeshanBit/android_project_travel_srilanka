package com.treinetic.app.travelsrilanka;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

public class DrawerActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    Fragment fragment;
    int selected_nav_tab;
    Menu menuObj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_activity_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent getIntentfromLogin = getIntent();
        Intent getIntentfromContinueAsGuest = getIntent();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        this.menuObj = menu;

//        if(selected_nav_tab == R.id.nav_home){
//
//            getMenuInflater().inflate(R.menu.toolbar_menu_home, menu);
//
//        }else if(selected_nav_tab == R.id.nav_accommodation){
//
//            getMenuInflater().inflate(R.menu.toolbar_menu_accommodation, menu);
//
//        }else if(selected_nav_tab == R.id.nav_vehical_hire){
//
//            getMenuInflater().inflate(R.menu.toolbar_menu_vehicle_hire, menu);
//
//        }else {
//
//            getMenuInflater().inflate(R.menu.toolbar_menu_home, menu);
//
//        }

        return true;
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        selected_nav_tab = item.getItemId();

        if (selected_nav_tab == R.id.nav_home) {

            fragment = new HomeFragment();

            getMenuInflater().inflate(R.menu.toolbar_menu_home,this.menuObj);

            LinearLayout linearLayoutToolbarAccommodation = (LinearLayout) findViewById(R.id.layout_ButtonPanel_Accommodation_Toolbar);
            linearLayoutToolbarAccommodation.setVisibility(LinearLayout.GONE);

            LinearLayout linearLayoutToolbarVehicleHire = (LinearLayout) findViewById(R.id.layout_ButtonPanel_VehicleHire_Toolbar);
            linearLayoutToolbarVehicleHire.setVisibility(LinearLayout.GONE);

            setTitle("Home");

        } else if (selected_nav_tab == R.id.nav_accommodation) {

            fragment = new AccommodationFragment();

            getSupportActionBar().setDisplayHomeAsUpEnabled(false);

            getMenuInflater().inflate(R.menu.toolbar_menu_accommodation,this.menuObj);

            LinearLayout linearLayoutToolbarAccommodation = (LinearLayout) findViewById(R.id.layout_ButtonPanel_Accommodation_Toolbar);
            linearLayoutToolbarAccommodation.setVisibility(LinearLayout.VISIBLE);

            LinearLayout linearLayoutToolbarVehicleHire = (LinearLayout) findViewById(R.id.layout_ButtonPanel_VehicleHire_Toolbar);
            linearLayoutToolbarVehicleHire.setVisibility(LinearLayout.GONE);

            setTitle("Accommodation");

        } else if (selected_nav_tab == R.id.nav_vehical_hire) {

            fragment = new VehicleHireFragment();

            getSupportActionBar().setDisplayHomeAsUpEnabled(false);

            getMenuInflater().inflate(R.menu.toolbar_menu_vehicle_hire,this.menuObj);

            LinearLayout linearLayoutToolbarAccommodation = (LinearLayout) findViewById(R.id.layout_ButtonPanel_Accommodation_Toolbar);
            linearLayoutToolbarAccommodation.setVisibility(LinearLayout.GONE);

            LinearLayout linearLayoutToolbarVehicleHire = (LinearLayout) findViewById(R.id.layout_ButtonPanel_VehicleHire_Toolbar);
            linearLayoutToolbarVehicleHire.setVisibility(LinearLayout.VISIBLE);

            setTitle("Vehicle Hire");

        }else if (selected_nav_tab == R.id.nav_post_ads) {

            fragment = new PostAddsFragment();

            LinearLayout linearLayoutToolbarAccommodation = (LinearLayout) findViewById(R.id.layout_ButtonPanel_Accommodation_Toolbar);
            linearLayoutToolbarAccommodation.setVisibility(LinearLayout.GONE);

            LinearLayout linearLayoutToolbarVehicleHire = (LinearLayout) findViewById(R.id.layout_ButtonPanel_VehicleHire_Toolbar);
            linearLayoutToolbarVehicleHire.setVisibility(LinearLayout.GONE);

            setTitle("Post Your Adds");

        } else if (selected_nav_tab == R.id.nav_options) {

        }else if (selected_nav_tab == R.id.nav_about) {

        }else if (selected_nav_tab == R.id.nav_logout) {

        }



        FragmentManager fragmentManager =getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content,fragment).commit();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
