package com.treinetic.app.travelsrilanka;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Desktop_User_2 on 7/24/2017.
 */


public class AccommodationFragment extends Fragment {

    TabLayout accommodation_TabLayout;
    ViewPager accommodation_ViewPager;
    AccommodationViewPagerAdapter accommodation_ViewPagerAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.accommodation_fragment_layout,container,false);

        accommodation_TabLayout = (TabLayout) v.findViewById(R.id.tabLayout_Accommodation);
        accommodation_ViewPager = (ViewPager) v.findViewById(R.id.viewPager_Accommodation);

        accommodation_ViewPagerAdapter = new AccommodationViewPagerAdapter(((AppCompatActivity)getActivity()).getSupportFragmentManager());
        accommodation_ViewPagerAdapter.addFragments(new AccommodationTabAllFragment(),"All");
        accommodation_ViewPagerAdapter.addFragments(new AccommodationTabBungalowsFragment(),"Bangalows");
        accommodation_ViewPagerAdapter.addFragments(new AccommodationTabHotelsFragment(),"Hotels");
        accommodation_ViewPagerAdapter.addFragments(new AccommodationTabHolidayHomesFragment(),"Holiday Homes");
        accommodation_ViewPagerAdapter.addFragments(new AccommodationTabResortsFragment(),"Resorts");

        accommodation_ViewPager.setAdapter(accommodation_ViewPagerAdapter);
        accommodation_TabLayout.setupWithViewPager(accommodation_ViewPager);

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();



    }
}
