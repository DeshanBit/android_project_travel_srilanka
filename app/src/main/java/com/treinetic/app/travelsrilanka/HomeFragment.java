package com.treinetic.app.travelsrilanka;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Desktop_User_2 on 7/24/2017.
 */

public class HomeFragment extends Fragment {

    TabLayout home_TabLayout;
    ViewPager home_ViewPager;
    HomeViewPagerAdapter home_ViewPagerAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment_layout,container,false);
    }

    @Override
    public void onStart() {
        super.onStart();

        home_TabLayout = (TabLayout) getView().findViewById(R.id.tabLayout_Home);
        home_ViewPager = (ViewPager) getView().findViewById(R.id.viewPager_Home);

        home_ViewPagerAdapter = new HomeViewPagerAdapter(((AppCompatActivity)getActivity()).getSupportFragmentManager());
        home_ViewPagerAdapter.addFragments(new HomeTabJourneysForYouFragment(),"Journeys For You");
        home_ViewPagerAdapter.addFragments(new HomeTabJourneysYouCreatedFragment(),"Journeys You Created");


        home_ViewPager.setAdapter(home_ViewPagerAdapter);
        home_TabLayout.setupWithViewPager(home_ViewPager);

    }
}
