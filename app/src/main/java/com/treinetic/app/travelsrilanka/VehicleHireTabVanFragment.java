package com.treinetic.app.travelsrilanka;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Desktop_User_2 on 7/25/2017.
 */

public class VehicleHireTabVanFragment extends Fragment {

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.vehicle_hire_fragment_tab_van_layout,container,false);
    }

    @Override
    public void onStart() {
        super.onStart();

        //============== TEST ======================
        recyclerView = (RecyclerView) getView().findViewById(R.id.recyclerView_TabVan_VehicleHire_Fragment);
        ZTestData zTestData = new ZTestData();
        adapter = new VehicleHireFragmentCardAdapter(zTestData.vehiclePic,zTestData.vehicleName,zTestData.vehicleType,
                zTestData.vehicleAC,zTestData.vehicleDriver,zTestData.vehiclePassengers,zTestData.vehicleFare,this.getContext());
        layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        //====================================

    }
}
