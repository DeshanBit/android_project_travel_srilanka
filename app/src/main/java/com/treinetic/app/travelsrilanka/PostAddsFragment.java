package com.treinetic.app.travelsrilanka;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Desktop_User_2 on 7/24/2017.
 */

public class PostAddsFragment  extends Fragment{

    TabLayout postAdds_TabLayout;
    ViewPager postAdds_ViewPager;
    PostAddsViewPagerAdapter postAddsViewPagerAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.post_ads_fragment_layout,container,false);
    }

    @Override
    public void onStart() {
        super.onStart();

        postAdds_TabLayout = (TabLayout) getView().findViewById(R.id.tabLayout_PostAdds);
        postAdds_ViewPager = (ViewPager) getView().findViewById(R.id.viewPager_PostAdds);

        postAddsViewPagerAdapter = new PostAddsViewPagerAdapter(((AppCompatActivity)getActivity()).getSupportFragmentManager());
        postAddsViewPagerAdapter.addFragments(new PostAddsTabYourAddsFragment(),"Your Adds");
        postAddsViewPagerAdapter.addFragments(new PostAddsTabCreateNewAddFragment(),"Create New Add");

        postAdds_ViewPager.setAdapter(postAddsViewPagerAdapter);
        postAdds_TabLayout.setupWithViewPager(postAdds_ViewPager);
    }
}
