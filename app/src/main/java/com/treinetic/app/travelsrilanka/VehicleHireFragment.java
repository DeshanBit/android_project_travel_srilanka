package com.treinetic.app.travelsrilanka;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Desktop_User_2 on 7/24/2017.
 */


public class VehicleHireFragment extends Fragment{

    TabLayout vehicleHire_TabLayout;
    ViewPager vehicleHire_ViewPager;
    VehicleHireViewPagerAdapter vehicleHire_ViewPagerAdapter;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v =  inflater.inflate(R.layout.vehicle_hire_fragment_layout,container,false);

        vehicleHire_TabLayout = (TabLayout) v.findViewById(R.id.tabLayout_VehicleHire);
        vehicleHire_ViewPager = (ViewPager) v.findViewById(R.id.viewPager_VehicleHire);

        vehicleHire_ViewPagerAdapter = new VehicleHireViewPagerAdapter(((AppCompatActivity) getActivity())
                .getSupportFragmentManager());
        vehicleHire_ViewPagerAdapter.addFragments(new VehicleHireTabAllFragment(), "All");
        vehicleHire_ViewPagerAdapter.addFragments(new VehicleHireTabVanFragment(), "Vans");
        vehicleHire_ViewPagerAdapter.addFragments(new VehicleHireTabCarFragment(), "Cars");
        vehicleHire_ViewPagerAdapter.addFragments(new VehicleHireTabBusFragment(), "Buses");

        vehicleHire_ViewPager.setAdapter(vehicleHire_ViewPagerAdapter);
        vehicleHire_TabLayout.setupWithViewPager(vehicleHire_ViewPager);

        return v;
    }


    @Override
    public void onStart() {
        super.onStart();


    }

}
