package com.treinetic.app.travelsrilanka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity_layout);
        getWindow().setBackgroundDrawableResource(R.drawable.login_background);
    }

    public void onClickLoginButton(View view){
        Intent loginIntent = new Intent(this,DrawerActivity.class);
        startActivity(loginIntent);
    }

    public void onClickContinueAsGuestButton(View view){
        Intent loginIntent = new Intent(this,DrawerActivity.class);
        startActivity(loginIntent);
    }

    public void onClickSignUpNowTextButton(View view){
        Intent signUpNowIntent = new Intent(this,SignUpActivity.class);
        startActivity(signUpNowIntent);
    }

}
