package com.treinetic.app.travelsrilanka;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Desktop_User_2 on 7/25/2017.
 */

public class PostAddsTabCreateNewAddFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.post_ads_fragment_tab_create_new_add_layout,container,false);
    }
}
