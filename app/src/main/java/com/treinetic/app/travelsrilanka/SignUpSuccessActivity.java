package com.treinetic.app.travelsrilanka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class SignUpSuccessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_success_activity_layout);
        getWindow().setBackgroundDrawableResource(R.drawable.signupsuccess_background);
        Intent getIntentFromSignUp = getIntent();
    }

    public void onClickBackToLoginButton(View view){
        Intent intentBack = new Intent();
        setResult(RESULT_OK,intentBack);
        finish();
    }
}
