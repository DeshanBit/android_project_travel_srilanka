package com.treinetic.app.travelsrilanka;

/**
 * Created by Desktop_User_2 on 7/27/2017.
 */

public class ZTestData {



    String[] vehiclePic = {"http://patpat.lk/uploads/c47f28a87e6f04baeb31e4ea6c93876e.jpg",
                           "http://www.transitrentacars.com/images/stories/toyota-hiace-kdh-200_1.jpg",
                           "https://www.whatcar.com/car-leasing/images/vehicles/medium/100892.jpg",
                           "https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/2012_Jeep_Liberty_--_NHTSA.jpg/1200px-2012_Jeep_Liberty_--_NHTSA.jpg",
                           "http://www.cabhiredelhi.com/uploads/mitsubishi-rosa-bus-hire-in-delhi-MITSUBISHI_ROSA_BUS_29_SEATER.jpg",
                           "http://patpat.lk/uploads/c47f28a87e6f04baeb31e4ea6c93876e.jpg",
                           "http://www.transitrentacars.com/images/stories/toyota-hiace-kdh-200_1.jpg",
                           "https://www.whatcar.com/car-leasing/images/vehicles/medium/100892.jpg",
                           "https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/2012_Jeep_Liberty_--_NHTSA.jpg/1200px-2012_Jeep_Liberty_--_NHTSA.jpg",
                           "http://www.cabhiredelhi.com/uploads/mitsubishi-rosa-bus-hire-in-delhi-MITSUBISHI_ROSA_BUS_29_SEATER.jpg"};

    String[] vehicleName= {"Toyota KDH 200 Super GL",
                           "Toyota KDH 200 Super GL",
                           "Toyota KDH 200 Super GL",
                           "Toyota KDH 200 Super GL",
                           "Toyota KDH 200 Super GL",
                           "Toyota KDH 200 Super GL",
                           "Toyota KDH 200 Super GL",
                           "Toyota KDH 200 Super GL",
                           "Toyota KDH 200 Super GL",
                           "Toyota KDH 200 Super GL"};

    String[] vehicleType = {"Van","Car","Bus","Van","Car","Bus","Van","Car","Bus","Van"};

    String[] vehicleAC = {"A/C","Non A/C","A/C","Non A/C","A/C", "Non A/C","A/C","Non A/C","A/C","Non A/C"};

    String[] vehicleDriver = {"With Driver","With Out Driver","With Driver","With Out Driver",
                              "With Driver","With Out Driver","With Driver","With Out Driver",
                              "With Driver","With Out Driver"};

    String[] vehiclePassengers = {"12 Passengers","15 Passengers","11 Passengers","10 Passengers","9 Passengers",
                                  "12 Passengers","15 Passengers","11 Passengers","10 Passengers","9 Passengers"};

    String[] vehicleFare = {"Rs 55.00","Rs 45.00","Rs 60.00","Rs 55.00","Rs 45.00",
                            "Rs 55.00","Rs 45.00","Rs 60.00","Rs 55.00","Rs 45.00"};




    String[] accommodationCardBackground = {"http://www.mysrilankaholidays.com/gallery/heritance-kandalama-hotel-sri-lanka-1.jpg",
                            "https://t-ec.bstatic.com/images/hotel/max1024x768/985/98522872.jpg",
                            "http://ceylonrealestate.net/news/wp-content/uploads/2014/09/Luxury-Hotel.jpg",
                            "http://mysrilankaholidays.com/hotelguide/wp-content/uploads/2012/06/malu-malu-resorts-and-Spa.jpg",
                            "https://djt1w80tgtcnw.cloudfront.net/uploadfiles/hotelimages/version2/large/TheGrandHotel_NuwaraEliya_SriLanka_SH0523_47804.jpg"};

    String[] accommodationName= {"Heritance Kandalama",
                            "Sigiriya Hotel",
                            "Heritance Kandalama",
                            "Sigiriya Hotel",
                            "Heritance Kandalama"};

    String[] accommodationType = {"Hotel","Resort","Holiday Home","Banglow","Hotel"};

    String[] accommodationLocation = {"Dambulla","Dambulla","Dambulla","Dambulla","Dambulla"};

    String[] accommodationCharging = {"Rs 2500 to 8000","Rs 2500 to 8000","Rs 2500 to 8000","Rs 2500 to 8000","Rs 2500 to 8000"};


}
