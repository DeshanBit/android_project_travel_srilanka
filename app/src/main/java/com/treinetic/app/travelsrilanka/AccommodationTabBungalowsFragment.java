package com.treinetic.app.travelsrilanka;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Desktop_User_2 on 7/24/2017.
 */

public class AccommodationTabBungalowsFragment extends Fragment {

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.accommodation_fragment_tab_bungalows_layout,container,false);
    }

    @Override
    public void onStart() {
        super.onStart();

        //============== TEST ======================
        recyclerView = (RecyclerView) getView().findViewById(R.id.recyclerView_TabBungalows_Accommodation_Fragment);
        ZTestData zTestData = new ZTestData();
        adapter = new AccommodationFragmentCardAdapter(zTestData.accommodationCardBackground,zTestData.accommodationName,zTestData.accommodationType,
                zTestData.accommodationLocation,zTestData.accommodationCharging,this.getContext());
        layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        //====================================

    }

}
