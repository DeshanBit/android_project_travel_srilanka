package com.treinetic.app.travelsrilanka;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * Created by Desktop_User_2 on 7/27/2017.
 */

public class AccommodationFragmentCardAdapter extends RecyclerView.Adapter<AccommodationFragmentCardAdapter.RecyclerViewHolder>{


    Context context;
    String[] accommodationCardBackground,accommodationName,accommodationType,accommodationLocation,accommodationCharging;




    public AccommodationFragmentCardAdapter(String accommodationCardBackground[],String accommodationName[],String accommodationType[],
                                            String accommodationLocation[],String accommodationCharging[],Context context){

        this.accommodationCardBackground = accommodationCardBackground;
        this.accommodationName = accommodationName;
        this.accommodationType = accommodationType;
        this.accommodationLocation = accommodationLocation;
        this.accommodationCharging = accommodationCharging;

        this.context = context;

    }


    @Override
    public AccommodationFragmentCardAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.accommodation_fragment_card,parent,false);
        AccommodationFragmentCardAdapter.RecyclerViewHolder recyclerViewHolder = new AccommodationFragmentCardAdapter.RecyclerViewHolder(view);

        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {



        Picasso.with(context).load(accommodationCardBackground[position]).into(new Target(){

            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                holder.accomodation_card_Layout.setBackground(new BitmapDrawable(context.getResources(), bitmap));
            }

            @Override
            public void onBitmapFailed(final Drawable errorDrawable) {
                Log.d("TAG", "FAILED");
            }

            @Override
            public void onPrepareLoad(final Drawable placeHolderDrawable) {
                Log.d("TAG", "Prepare Load");
            }
        });


        holder.accommodation_name_txt.setText(accommodationName[position]);
        holder.accommodation_type_txt.setText(accommodationType[position]);
        holder.accommodation_location_txt.setText(accommodationLocation[position]);
        holder.accommodation_charging_txt.setText(accommodationCharging[position]);

        holder.accomodation_card_Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent accommodationDetailIntent = new Intent(context,AccommodationDetailActivity.class);
                context.startActivity(accommodationDetailIntent);

            }
        });

    }

    @Override
    public int getItemCount() { return accommodationName.length; }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder {

        View accomodation_card_Layout;

        TextView accommodation_name_txt;
        TextView accommodation_type_txt;
        TextView accommodation_location_txt;
        TextView accommodation_charging_txt;

        public RecyclerViewHolder(View itemView) {
            super(itemView);

            accomodation_card_Layout = itemView.findViewById(R.id.layout_Background_Accommodation_Card);

            accommodation_name_txt = (TextView) itemView.findViewById(R.id.txtView_Name_Accommodation_Card);
            accommodation_type_txt = (TextView) itemView.findViewById(R.id.txtView_Type_Accommodation_Card);
            accommodation_location_txt = (TextView) itemView.findViewById(R.id.txtView_Location_Accommodation_Card);
            accommodation_charging_txt = (TextView) itemView.findViewById(R.id.txtView_Charging_Accommodation_Card);

        }

    }
}
