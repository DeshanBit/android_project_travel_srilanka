package com.treinetic.app.travelsrilanka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class SignUpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity_layout);
        getWindow().setBackgroundDrawableResource(R.drawable.signup_background);
        Intent getIntentFromSignUpNow = getIntent();
    }

    public void onClickSignUpButton(View view){
        Intent signUpIntent = new Intent(this,SignUpSuccessActivity.class);
        startActivity(signUpIntent);
        finish();
    }


}
