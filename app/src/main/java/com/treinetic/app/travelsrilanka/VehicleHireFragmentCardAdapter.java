package com.treinetic.app.travelsrilanka;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by Desktop_User_2 on 7/27/2017.
 */

public class VehicleHireFragmentCardAdapter extends RecyclerView.Adapter<VehicleHireFragmentCardAdapter.RecyclerViewHolder> {

    Context context;
    String[] vehiclePic,vehicleName,vehicleType,vehicleAC,vehicleDriver,vehiclePassengers,vehicleFare;

    public VehicleHireFragmentCardAdapter(String vehiclePic[], String vehicleName[], String vehicleType[], String vehicleAC[],
                                          String vehicleDriver[], String vehiclePassengers[], String vehicleFare[], Context context){

        this.vehiclePic = vehiclePic;
        this.vehicleName = vehicleName;
        this.vehicleType = vehicleType;
        this.vehicleAC = vehicleAC;
        this.vehicleDriver = vehicleDriver;
        this.vehiclePassengers = vehiclePassengers;
        this.vehicleFare = vehicleFare;

        this.context = context;

    }

    @Override
    public VehicleHireFragmentCardAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vehicle_hire_fragment_card,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);

        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(VehicleHireFragmentCardAdapter.RecyclerViewHolder holder, int position) {

        Picasso.with(context).load(vehiclePic[position]).into(holder.vehicle_pic_img);
        holder.vehicle_name_txt.setText(vehicleName[position]);
        holder.vehicle_type_txt.setText(vehicleType[position]);
        holder.vehicle_ac_txt.setText(vehicleAC[position]);
        holder.vehicle_driver_txt.setText(vehicleDriver[position]);
        holder.vehicle_passengers_txt.setText(vehiclePassengers[position]);
        holder.vehicle_fare_txt.setText(vehicleFare[position]);

        holder.vehicle_parent_card_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent vehicleHireDetailIntent = new Intent(context,VehicleHireDetailActivity.class);
                context.startActivity(vehicleHireDetailIntent);

            }
        });


    }

    @Override
    public int getItemCount() { return vehicleName.length; }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder{

        ImageView vehicle_pic_img;
        TextView vehicle_name_txt;
        TextView vehicle_type_txt;
        TextView vehicle_ac_txt;
        TextView vehicle_driver_txt;
        TextView vehicle_passengers_txt;
        TextView vehicle_fare_txt;

        View vehicle_parent_card_layout;

        public RecyclerViewHolder(View itemView) {
            super(itemView);

            this.vehicle_pic_img = (ImageView) itemView.findViewById(R.id.img_VehicleHire_Card);
            this.vehicle_name_txt = (TextView) itemView.findViewById(R.id.txtView_VehicleName_VehicleHire_Card);
            this.vehicle_type_txt = (TextView) itemView.findViewById(R.id.txtView_VehicleType_VehicleHire_Card);
            this.vehicle_ac_txt = (TextView) itemView.findViewById(R.id.txtView_VehicleAC_VehicleHire_Card);
            this.vehicle_driver_txt = (TextView) itemView.findViewById(R.id.txtView_VehicleDriver_VehicleHire_Card);
            this.vehicle_passengers_txt = (TextView) itemView.findViewById(R.id.txtView_VehiclePassengers_VehicleHire_Card);
            this.vehicle_fare_txt = (TextView) itemView.findViewById(R.id.txtView_VehicleFare_VehicleHire_Card);

            this.vehicle_parent_card_layout = itemView.findViewById(R.id.layout_parent_VehicleHire_Card);

        }
    }
}
